# torrent

![](https://img.shields.io/badge/written%20in-PHP-blue)

A command-line tool to search multiple sources and add torrents to transmission.

- Supports TPB and Nyaa
- Colour highlighting for trusted uploaders / verified torrents
- Can be modified to perform any action with the recieived torrent

Tags: torrent, anime, scraper


## Download

- [⬇️ torrent-r1.zip](dist-archive/torrent-r1.zip) *(2.50 KiB)*
